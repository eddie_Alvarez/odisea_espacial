package clases;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Game_Settings extends JPanel {
	public JTextField txtTime;
	JButton btnSave = new JButton("Guardar");
	JButton btnCancel = new JButton("Cancelar");
	JCheckBox chckIncrease = new JCheckBox("");
	JCheckBox chckExtraPoints = new JCheckBox("");
	JCheckBox chckSpeedIncrease = new JCheckBox("");
	JCheckBox checkDecreaseTime = new JCheckBox("");
	JCheckBox chckPenalty = new JCheckBox("");
	JCheckBox chckFreezing = new JCheckBox("");
	JComboBox cmbFrequency = new JComboBox();
	JComboBox cmbLevel = new JComboBox();
	
	public Game_Settings() {
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.WEST);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblItems = new JLabel("Items");
		panel_1.add(lblItems);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblAumentoDeTiempo = new JLabel("Aumento de Tiempo");
		panel_2.add(lblAumentoDeTiempo);
		chckIncrease.setSelected(true);
		panel_2.add(chckIncrease);
		
		JPanel panel_3 = new JPanel();
		panel.add(panel_3);
		panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblPuntosExtra = new JLabel("Puntos extra");
		panel_3.add(lblPuntosExtra);
		chckExtraPoints.setSelected(true);
		panel_3.add(chckExtraPoints);
		
		JPanel panel_4 = new JPanel();
		panel.add(panel_4);
		panel_4.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblAumentoDeVelocidad = new JLabel("Aumento de velocidad");
		panel_4.add(lblAumentoDeVelocidad);
		chckSpeedIncrease.setSelected(true);
		panel_4.add(chckSpeedIncrease);
		
		JPanel panel_5 = new JPanel();
		panel.add(panel_5);
		panel_5.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblDisminucinDeTiempo = new JLabel("Disminuci\u00F3n de tiempo");
		panel_5.add(lblDisminucinDeTiempo);
		checkDecreaseTime.setSelected(true);
		panel_5.add(checkDecreaseTime);
		
		JPanel panel_6 = new JPanel();
		panel.add(panel_6);
		panel_6.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblPenalizacin = new JLabel("Penalizaci\u00F3n");
		panel_6.add(lblPenalizacin);
		chckPenalty.setSelected(true);
		panel_6.add(chckPenalty);
		
		JPanel panel_7 = new JPanel();
		panel.add(panel_7);
		panel_7.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblCongelacin = new JLabel("Congelaci\u00F3n");
		panel_7.add(lblCongelacin);
		chckFreezing.setSelected(true);
		panel_7.add(chckFreezing);
		
		JPanel panel_8 = new JPanel();
		panel.add(panel_8);
		panel_8.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblFrecuencia = new JLabel("Frecuencia");
		panel_8.add(lblFrecuencia);
		cmbFrequency.addItem("Normal");
		cmbFrequency.addItem("Poco frecuente");
		cmbFrequency.addItem("Muy Frecuente");
		panel_8.add(cmbFrequency);
		
		JPanel panel_9 = new JPanel();
		add(panel_9, BorderLayout.CENTER);
		panel_9.setLayout(new BoxLayout(panel_9, BoxLayout.Y_AXIS));
		
		JPanel panel_10 = new JPanel();
		panel_9.add(panel_10);
		panel_10.setLayout(new BoxLayout(panel_10, BoxLayout.Y_AXIS));
		
		JPanel panel_11 = new JPanel();
		panel_10.add(panel_11);
		
		JLabel lblVelocidad = new JLabel("Velocidad");
		panel_11.add(lblVelocidad);
		
		JPanel panel_12 = new JPanel();
		panel_10.add(panel_12);
		
		JLabel lblNivel = new JLabel("Nivel");
		panel_12.add(lblNivel);
		
		cmbLevel.addItem("Normal");
		cmbLevel.addItem("Rapido");
		panel_12.add(cmbLevel);
		
		JPanel panel_13 = new JPanel();
		panel_9.add(panel_13);
		panel_13.setLayout(new BoxLayout(panel_13, BoxLayout.Y_AXIS));
		
		JPanel panel_14 = new JPanel();
		panel_13.add(panel_14);
		
		JLabel lblNewLabel = new JLabel("Duraci\u00F3n");
		panel_14.add(lblNewLabel);
		
		JPanel panel_15 = new JPanel();
		panel_13.add(panel_15);
		
		JLabel lblTiempo = new JLabel("Tiempo");
		panel_15.add(lblTiempo);
		
		txtTime = new JTextField("1:30");
		panel_15.add(txtTime);
		txtTime.setColumns(10);
		
		JPanel panel_16 = new JPanel();
		panel_9.add(panel_16);
		
		panel_16.add(btnSave);
		
		panel_16.add(btnCancel);

	}
	
	public ArrayList loadItems(){
		ArrayList<JLabel> items = new ArrayList<JLabel>();
		if(chckIncrease.isSelected() == true){
			Item item1 = new Item(1);
			items.add(item1);
		}
		if(chckExtraPoints.isSelected() == true){
			Item item2 = new Item(2);
			items.add(item2);
		}
		if(chckSpeedIncrease.isSelected() == true){
			Item item3 = new Item(3);
			items.add(item3);
		}
		if(checkDecreaseTime.isSelected() == true){
			Item item4 = new Item(4);
			items.add(item4);
		}
		if(chckPenalty.isSelected() == true){
			Item item5 = new Item(5);
			items.add(item5);
		}
		if(chckFreezing.isSelected() == true){
			Item item6 = new Item(6);
			items.add(item6);
		}
		return items;
	}
	
	public String getTextFrequency(){
		return (String) cmbFrequency.getSelectedItem();
	}
	
	public String getTextLevel(){
		return (String) cmbLevel.getSelectedItem();
	}
	public int getDurationMinutes(){
		StringTokenizer delimiterHour = new StringTokenizer(txtTime.getText(), ":");
		int minutes = 0;
		while(delimiterHour.hasMoreTokens()){
			 minutes = Integer.valueOf(delimiterHour.nextToken());
			 break;
		}
		return minutes;
	}
	
	public int getDurationSeconds(){
		StringTokenizer delimiterHour = new StringTokenizer(txtTime.getText(), ":");
		int seconds = 0;
		while(delimiterHour.hasMoreTokens()){
			delimiterHour.nextToken();
			seconds = Integer.valueOf(delimiterHour.nextToken());
		}
		return seconds;
	}
}
