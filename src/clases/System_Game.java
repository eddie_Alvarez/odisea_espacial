package clases;

import java.awt.Color;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;


public class System_Game {
	
	Main_Gui mainGui = new Main_Gui();
	Main_Menu mainMenu = new Main_Menu();
	Game_Settings gameSettings = new Game_Settings();
	Panel_Game panelGame = new Panel_Game();
	
	ArrayList<Player> playerList = new ArrayList<Player>();
	ArrayList<JLabel> itemsList;
	ArrayList<Item> iteminGame = new ArrayList<Item>();
	ArrayList<JLabel> shootList = new ArrayList<JLabel>();
	
	Timer timer;
	Timer timerItems;
	Timer movementItem;
	Timer movementEnemies;
	Timer timerReloj;
	
	Thread hilo;
	Thread threadItem;
	
	int min;
	int seg;
	int speedEnemy;
	int shipSpeed;
	int speedShoot;
	int speedItem;
	int frequencyItem;
	int countBoostShip;
	int countFreezing;
	int points = 0;
	
	boolean movement = true;
	boolean boostShip;
	boolean freezingShip;
	boolean result;
	boolean pause = false;

	
	public System_Game(){
		addListener();
		addPane();
	}
	
	public void addPane(){
		mainGui.addPane(mainMenu, "mainMenu");
		mainGui.addPane(gameSettings, "gameSettings");
		mainGui.addPane(panelGame, "panelGame");
		mainGui.showPane("mainMenu");
	}
	
	public void addListener(){
		mainMenu.btnExit.addActionListener(clickExit);
		mainMenu.btnConfiguration.addActionListener(clickGameSettings);
		mainMenu.btnNewGame.addActionListener(clickNewGame);
		panelGame.addKeyListener(movementShip);
		gameSettings.btnSave.addActionListener(clickSave);
		gameSettings.btnCancel.addActionListener(clickCancel);
	}
	
	ActionListener clickExit = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			mainGui.mainFrame.dispose();
		}
		
	};
	
	
	ActionListener clickGameSettings = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			mainGui.showPane("gameSettings");
		}
		
	};
	
	ActionListener clickSave = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			mainGui.showPane("mainMenu");
		}
		
	};
	
	ActionListener clickCancel = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			gameSettings.chckIncrease.setSelected(true);
			gameSettings.chckExtraPoints.setSelected(true);
			gameSettings.chckSpeedIncrease.setSelected(true);
			gameSettings.checkDecreaseTime.setSelected(true);
			gameSettings.chckPenalty.setSelected(true);
			gameSettings.chckFreezing.setSelected(true);
			gameSettings.cmbFrequency.setSelectedItem("Normal");
			gameSettings.cmbLevel.setSelectedItem("Normal");
			gameSettings.txtTime.setText("1:30");
			mainGui.showPane("mainMenu");
		}
		
	};
	
	ActionListener clickNewGame = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			mainGui.showPane("panelGame");
			itemsList = gameSettings.loadItems();
			min = gameSettings.getDurationMinutes();
			seg = gameSettings.getDurationSeconds();
			panelGame.lblTime.setText(gameSettings.txtTime.getText());
			if(gameSettings.cmbLevel.getSelectedItem() == "Normal"){
				shipSpeed = 15;
				speedShoot = 7;
				speedEnemy = 300;
				speedItem = 50;
			}else{
				shipSpeed = 20;
				speedShoot = 4;
				speedEnemy = 210;
				speedItem = 20;
			}
			if(gameSettings.cmbFrequency.getSelectedItem() == "Normal"){
				frequencyItem = 4000;
			}else if(gameSettings.cmbFrequency.getSelectedItem() == "Poco frecuente"){
				frequencyItem = 6000;
			}else{
				frequencyItem = 2000;
			}
			movementEnemies = new Timer(speedEnemy, timerEnemies);
			timerReloj = new Timer (1000, timerTime);
			threadItem = new Thread(itemtoPanel);
			movementEnemies.start();
			timerReloj.start();
			threadItem.start();
		}
		
	};
	
	KeyListener movementShip = new KeyListener(){

		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub;
		}
		
		@Override
		public void keyPressed(KeyEvent e) {
			if(freezingShip == false && pause == false || e.getKeyCode() == KeyEvent.VK_ESCAPE){
				int movement = panelGame.lblShip.getY();
				int x = panelGame.lblShip.getX();
				if(e.getKeyCode() == KeyEvent.VK_UP){
					movement = movement - shipSpeed;
					if(movement > 50){
						panelGame.lblShip.setLocation(x, movement);
					}
				}else if(e.getKeyCode() == KeyEvent.VK_DOWN){
					movement = movement + shipSpeed;
					if((movement + 48) < mainGui.panelCard.getSize().getHeight()){
						panelGame.lblShip.setLocation(x, movement);
					}
				}else if(e.getKeyCode() == KeyEvent.VK_SPACE){
					int tip = (int) (x + (panelGame.lblShip.getSize().getWidth()));
					int tipy = (int)(movement + 24);
					JLabel lblShooting = new JLabel("");
					lblShooting.setOpaque(true);
					lblShooting.setBackground(Color.WHITE);
					lblShooting.setBounds(tip, tipy, 10, 5);
					shootList.add(lblShooting);
					panelGame.add(lblShooting);
					panelGame.repaint();
					timer = new Timer(speedShoot, new ActionListener(){

						@Override
						public void actionPerformed(ActionEvent e) {
								int y = lblShooting.getY();
								int x = lblShooting.getX();
								x = x + 5;
								lblShooting.setLocation(x,y);
								if(collisionShoot(lblShooting) == true){
									panelGame.remove(lblShooting);
									panelGame.validate();
									panelGame.repaint();
									shootList.remove(lblShooting);
								}
								if((x + 10) == mainGui.panelCard.getSize().getWidth()){
									panelGame.remove(lblShooting);
									panelGame.repaint();
								}
						}
						
					});
					timer.start();
				}else if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
					if(pause == false){
						timerItems.stop();
						movementItem.stop();
						movementEnemies.stop();
						timerReloj.stop();
						pause = true;
					}else{
						timerItems.start();
						movementItem.start();
						movementEnemies.start();
						timerReloj.start();
						pause = false;
					}
				}
			}		
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub
		}

	};
	
	
	ActionListener timerEnemies = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			for(int i=0; i<5; i++){
				for(int k=0; k<8; k++){
					if(panelGame.enemyMatrix[k][i] != null){
						int x = panelGame.enemyMatrix[k][i].getX();
						int y = panelGame.enemyMatrix[k][i].getY();
						if(movement == true){
							if((panelGame.enemyMatrix[k][4].getY()-2) > panelGame.lblPanel.getHeight()){
								x = x - 2;
								panelGame.enemyMatrix[k][i].setLocation(x, y);
								y = y - 2;
								panelGame.enemyMatrix[k][i].setLocation(x, y);
								if(collisionShip(panelGame.enemyMatrix[k][i]) == true || x <= 0){
									result = false;
									win();
								}
							}else{
								//if(i == 4){
									movement = false;
									i = -1;
									break;
								//}	
							}
						}else{
							if((panelGame.enemyMatrix[k][4].getY() + 49) < mainGui.panelCard.getHeight()){
								x = x - 2;
								panelGame.enemyMatrix[k][i].setLocation(x, y);
								y = y + 2;
								panelGame.enemyMatrix[k][i].setLocation(x, y);
								if(collisionShip(panelGame.enemyMatrix[k][i]) == true || x <= 0){
									result = false;
									win();
								}
							}else{
									movement = true;
									i = -1;
									break;
							}
						}
					}
				}
			}
		}
		
	};
	
	public boolean collisionShip(JLabel enemy){
		Rectangle rectangleEnemy = new Rectangle(enemy.getBounds());
		Rectangle rectangleShip = new Rectangle(panelGame.lblShip.getBounds());
		return rectangleEnemy.intersects(rectangleShip);
	}
	
	public boolean collisionShoot(JLabel shoot){
		for(JLabel item: shootList){
			if(item == shoot){
				Rectangle rectangleShoot = new Rectangle(shoot.getBounds());
				for(int i=0; i<5; i++){
					for(int k=0; k<8; k++){
						if(panelGame.enemyMatrix[k][i] != null){
							Rectangle rectangleEnemy = new Rectangle(panelGame.enemyMatrix[k][i].getBounds());
							if(rectangleShoot.intersects(rectangleEnemy) == true){
								try{
								if(i == 0){
									Enemy_1 enemy = (Enemy_1) panelGame.enemyMatrix[k][i];
									enemy.setLife(enemy.getLife() - 1);
									if(enemy.getLife() == 0){
										panelGame.remove(panelGame.enemyMatrix[k][i]);
										panelGame.enemyMatrix[k][i] = null;
										points = points + 10;
										if(unlockTier2() == true){
											movementEnemies.setDelay((int)(speedEnemy/1.5));
											panelGame.lblSpeed.setText("x1.5");
										}
										panelGame.lblPts.setText(String.valueOf(points));
									}else{
										panelGame.enemyMatrix[k][i] = enemy;
									}
								}else if(i == 1 || i== 2){
									if(unlockTier2() == true){
										Enemy_2 enemy = (Enemy_2) panelGame.enemyMatrix[k][i];
										enemy.setLife(enemy.getLife() - 1);
										if(enemy.getLife() == 0){
											panelGame.remove(panelGame.enemyMatrix[k][i]);
											panelGame.enemyMatrix[k][i] = null;
											points = points + 20;
											if(unlockTier3() == true){
												movementEnemies.setDelay((speedEnemy/2));
												panelGame.lblSpeed.setText("x2");
											}
											panelGame.lblPts.setText(String.valueOf(points));
										}else{
											panelGame.enemyMatrix[k][i] = enemy;
										}
									}
								}else if( i == 3 || i == 4){
									if(unlockTier3() == true){
										Enemy_3 enemy = (Enemy_3) panelGame.enemyMatrix[k][i];
										enemy.setLife(enemy.getLife() - 1);
										if(enemy.getLife() == 0){
											panelGame.remove(panelGame.enemyMatrix[k][i]);
											panelGame.enemyMatrix[k][i] = null;
											points = points + 30;
											panelGame.lblPts.setText(String.valueOf(points));
											if(winCondition() == true){
												result = true;
												win();
											}
										}else{
											panelGame.enemyMatrix[k][i] = enemy;
										}
									}
								}
								return true;
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	public boolean unlockTier2(){
		for(int i=0; i<1; i++){
			for(int  j=0; j<8; j++){
				if(panelGame.enemyMatrix[j][i] != null){
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean unlockTier3(){
		for(int i=1; i<3; i++){
			for(int  j=0; j<8; j++){
				if(panelGame.enemyMatrix[j][i] != null){
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean winCondition(){
		for(int i=3; i<5; i++){
			for(int  j=0; j<8; j++){
				if(panelGame.enemyMatrix[j][i] != null){
					return false;
				}
			}
		}
		return true;
	}
	
	
	Runnable itemtoPanel = new Runnable(){

		@Override
		public void run() {
			spawnItem();
			movementItem();
		}
		
	};
	
	public void spawnItem(){
		 timerItems = new Timer(frequencyItem, new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				Random aleatorio = new Random(System.currentTimeMillis());
				int random = aleatorio.nextInt(itemsList.size() - 1);
				Item selectedItem = (Item) itemsList.get(random);
				aleatorio.setSeed(System.currentTimeMillis());
				int x = ThreadLocalRandom.current().nextInt(0, 1150);
				int y = ThreadLocalRandom.current().nextInt(51, 660);
				selectedItem.setBounds(x, y, 20, 20);
				if(selectedItem.getType() == 1){
					URL route = getClass().getResource("/resources/tiempoItem_201700326.png");
					ImageIcon image = new ImageIcon(route);
					ImageIcon icon = new ImageIcon(image.getImage().getScaledInstance(selectedItem.getWidth(), selectedItem.getHeight(), Image.SCALE_DEFAULT));
					selectedItem.setIcon(icon);
				}else if(selectedItem.getType() == 2){
					URL route = getClass().getResource("/resources/puntosmasItem_201700326.png");
					ImageIcon image = new ImageIcon(route);
					ImageIcon icon = new ImageIcon(image.getImage().getScaledInstance(selectedItem.getWidth(), selectedItem.getHeight(), Image.SCALE_DEFAULT));
					selectedItem.setIcon(icon);
				}else if(selectedItem.getType() == 3){
					URL route = getClass().getResource("/resources/velocidadItem_201700326.png");
					ImageIcon image = new ImageIcon(route);
					ImageIcon icon = new ImageIcon(image.getImage().getScaledInstance(selectedItem.getWidth(), selectedItem.getHeight(), Image.SCALE_DEFAULT));
					selectedItem.setIcon(icon);
				}else if(selectedItem.getType() == 4){
					URL route = getClass().getResource("/resources/puntosmenosItem_201700326.png");
					ImageIcon image = new ImageIcon(route);
					ImageIcon icon = new ImageIcon(image.getImage().getScaledInstance(selectedItem.getWidth(), selectedItem.getHeight(), Image.SCALE_DEFAULT));
					selectedItem.setIcon(icon);
				}else if(selectedItem.getType() == 5){
					URL route = getClass().getResource("/resources/lostTime_201700326.png");
					ImageIcon image = new ImageIcon(route);
					ImageIcon icon = new ImageIcon(image.getImage().getScaledInstance(selectedItem.getWidth(), selectedItem.getHeight(), Image.SCALE_DEFAULT));
					selectedItem.setIcon(icon);
				}else{
					URL route = getClass().getResource("/resources/congelado_201700326.png");
					ImageIcon image = new ImageIcon(route);
					ImageIcon icon = new ImageIcon(image.getImage().getScaledInstance(selectedItem.getWidth(), selectedItem.getHeight(), Image.SCALE_DEFAULT));
					selectedItem.setIcon(icon);
				}
				iteminGame.add(selectedItem);
				panelGame.add(selectedItem);
				panelGame.repaint();
			}
			
		});
		 timerItems.start();
	}
	
	public void movementItem(){
		movementItem = new Timer(speedItem, new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!iteminGame.isEmpty()){
					for(int i=0; i<iteminGame.size(); i++){
						int x = iteminGame.get(i).getX();
						int y = iteminGame.get(i).getY();
						x = x - 5;
						iteminGame.get(i).setLocation(x, y);
						if(collisionItem(iteminGame.get(i)) == true){
							if(iteminGame.get(i).getType() == 1){
								seg = seg + 10;
								updateLbl();
								panelGame.remove(iteminGame.get(i));
								panelGame.repaint();
								iteminGame.remove(i);
							}else if(iteminGame.get(i).getType() == 2){
								points = points + 10;
								panelGame.lblPts.setText(String.valueOf(points));
								panelGame.remove(iteminGame.get(i));
								panelGame.repaint();
								iteminGame.remove(i);
							}else if(iteminGame.get(i).getType() == 3){
								if(gameSettings.cmbLevel.getSelectedItem() == "Normal"){
									shipSpeed = 15; 
								}else{
									shipSpeed = 20;
								}
								shipSpeed = shipSpeed*2;
								boostShip = true;
								countBoostShip = 0;
								panelGame.remove(iteminGame.get(i));
								panelGame.repaint();
								iteminGame.remove(i);
							}else if(iteminGame.get(i).getType() == 4){
								if(min != 0){
									seg = seg - 10;
									if(seg <= 0){
										min--;
										seg = 60 - seg;
									}
									updateLbl();
								}else{
									seg = seg - 10;
									if(seg <= 0){
										result = false;
										panelGame.lblTime.setText("0:00");
										win();
									}else{
										updateLbl();
									}
								}
								panelGame.remove(iteminGame.get(i));
								panelGame.repaint();
								iteminGame.remove(i);
							}else if(iteminGame.get(i).getType() == 5){
								points = points - 10;
								panelGame.lblPts.setText(String.valueOf(points));
								panelGame.remove(iteminGame.get(i));
								panelGame.repaint();
								iteminGame.remove(i);
							}else{
								freezingShip = true;
								countFreezing = 0;
								panelGame.remove(iteminGame.get(i));
								panelGame.repaint();
								iteminGame.remove(i);
							}
						}
						if(x <= 0){
							panelGame.remove(iteminGame.get(i));
							panelGame.repaint();
							iteminGame.remove(i);
						}
					}
				}
			}
			
		});
		movementItem.start();
	}
	
	public boolean collisionItem(Item item){
		Rectangle rectangleItem = new Rectangle(item.getBounds());
		Rectangle rectabgleShip = new Rectangle(panelGame.lblShip.getBounds());
		if(rectangleItem.intersects(rectabgleShip) == true){
			return true;
		}
		return false;
	}
	
	ActionListener timerTime = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			if(seg != 0){
				seg--;
			}
			if(seg == 0 && min == 0){
				updateLbl();
				result = false;
				win();
			}else{
				if(seg == 0){
					seg = 60;
					min--;
				}
				if(boostShip == true){
					countBoostShip++;
					if(countBoostShip == 10){
						boostShip = false;
						countBoostShip = 0;
						shipSpeed = shipSpeed / 2;
					}
				}
				if(freezingShip == true){
					countFreezing++;
					if(countFreezing == 5){
						freezingShip = false;
						countFreezing = 0;
					}
				}
				updateLbl();
			}
			
		}
		
	};
	
	public void updateLbl(){
		String time =   min + ":" + (seg<=9?"0":"")+ seg;
		panelGame.lblTime.setText(time);
	}
	
	public void win(){
		timerItems.stop();
		movementItem.stop();
		movementEnemies.stop();
		timerReloj.stop();
		threadItem.interrupt();
		if(result == true){
			String nl = System.getProperty("line.separator");
			JOptionPane.showMessageDialog(null, "Felicidades ha ganado el juego"+ nl + "Su puntuacion es de " + points, "Felicidades", JOptionPane.INFORMATION_MESSAGE);
			String name = JOptionPane.showInputDialog("Ingrese su nombre");
			Player player = new Player(name, points);
			playerList.add(player);
			iteminGame.clear();
			shootList.clear();
			countBoostShip = 0;
			countFreezing = 0;
			points = 0;
			movement = true;
			boostShip = false;
			freezingShip = false;
			mainGui.showPane("mainMenu");
		}else{
			String nl = System.getProperty("line.separator");
			JOptionPane.showMessageDialog(null, "Derrota: ha perdido el juego"+ nl + "Su puntuacion es de " + points, "Derrota", JOptionPane.INFORMATION_MESSAGE);
			String name = JOptionPane.showInputDialog("Ingrese su nombre");
			Player player = new Player(name, points);
			playerList.add(player);
			iteminGame.clear();
			shootList.clear();
			countBoostShip = 0;
			countFreezing = 0;
			points = 0;
			movement = true;
			boostShip = false;
			freezingShip = false;
			mainGui.showPane("mainMenu");
		}
	}
	
}