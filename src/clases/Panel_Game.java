package clases;

import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.net.URL;
import java.awt.SystemColor;

public class Panel_Game extends JPanel {

	JLabel lblShip = new JLabel("");
	JLabel[][] enemyMatrix = new JLabel[8][5];
	JPanel lblPanel = new JPanel();
	JLabel lblPts = new JLabel("0");
	JLabel lblSpeed = new JLabel("x1");
	JLabel lblTime = new JLabel("");
	Image image;
	
	public Panel_Game() {
		setBackground(SystemColor.control);
		setLayout(null);
		
		URL route = getClass().getResource("/resources/nave_201700326.png");
		ImageIcon nave = new ImageIcon(route);
		ImageIcon icon = new ImageIcon(nave.getImage().getScaledInstance(69, 48, Image.SCALE_DEFAULT));
		
		lblShip.setBounds(10, 127, 69, 48);
		lblShip.setIcon(icon);
		add(lblShip);
		
		lblPanel.setBounds(0, 0, 1150, 48);
	
		add(lblPanel);
		lblPanel.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("TIEMPO");
		lblNewLabel_1.setBounds(582, 11, 46, 14);
		lblPanel.add(lblNewLabel_1);
		lblNewLabel_1.setBackground(Color.WHITE);
		
		lblTime.setBounds(582, 24, 46, 14);
		lblPanel.add(lblTime);
		
		JLabel lblNewLabel_3 = new JLabel("VELOCIDAD");
		lblNewLabel_3.setBounds(499, 11, 73, 14);
		lblPanel.add(lblNewLabel_3);
		
		
		lblSpeed.setBounds(510, 24, 46, 14);
		lblPanel.add(lblSpeed);
		
		
		lblPts.setBounds(443, 24, 46, 14);
		lblPanel.add(lblPts);
		
		JLabel lblNewLabel = new JLabel("PUNTOS");
		lblNewLabel.setBounds(437, 11, 52, 14);
		lblPanel.add(lblNewLabel);
		
		fillMatrix();
		
		image = new ImageIcon(getClass().getResource("/resources/fondo_201700326.jpg")).getImage();
		
		this.addComponentListener(new ComponentAdapter(){
			@Override
			public void componentShown( ComponentEvent e ) {
				Panel_Game.this.requestFocusInWindow();
	        }
		});
		
	}
	
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
	}
	
	public void fillMatrix(){
		for(int i=0; i<5; i++){
			for(int k=0; k<8; k++){
				if(i == 0){
					Enemy_1  enemy1 = new Enemy_1();
					enemy1.setBounds(680, 90 + (k*58), 69,48);
					URL route = getClass().getResource("/resources/enemy1_201700326.png");
					ImageIcon enemy_1 = new ImageIcon(route);
					ImageIcon icon = new ImageIcon(enemy_1.getImage().getScaledInstance(enemy1.getWidth(), enemy1.getHeight(), Image.SCALE_DEFAULT));
					enemy1.setIcon(icon);
					this.add(enemy1);
					enemyMatrix[k][i] = enemy1;
				}
				if(i == 1 || i == 2){
					Enemy_2  enemy2 = new Enemy_2();
					enemy2.setBounds((680 + (90 * i)), 90 + (k*58), 69,48);
					URL route = getClass().getResource("/resources/enemy2_201700326.png");
					ImageIcon enemy_2 = new ImageIcon(route);
					ImageIcon icon = new ImageIcon(enemy_2.getImage().getScaledInstance(enemy2.getWidth(), enemy2.getHeight(), Image.SCALE_DEFAULT));
					enemy2.setIcon(icon);
					this.add(enemy2);
					enemyMatrix[k][i] = enemy2;
				}
				if(i == 3 || i == 4){
					Enemy_3  enemy3 = new Enemy_3();
					enemy3.setBounds(680 + (i*90), 90 + (k*58), 69,48);
					URL route = getClass().getResource("/resources/enemy3_201700326.png");
					ImageIcon enemy_3 = new ImageIcon(route);
					ImageIcon icon = new ImageIcon(enemy_3.getImage().getScaledInstance(enemy3.getWidth(), enemy3.getHeight(), Image.SCALE_DEFAULT));
					enemy3.setIcon(icon);
					this.add(enemy3);
					enemyMatrix[k][i] = enemy3;
				}
			}
		}
	}
}
