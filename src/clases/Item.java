package clases;

import javax.swing.JLabel;

public class Item extends JLabel{
	private int type;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Item(int type) {
		super();
		this.type = type;
	}
	
}
