package clases;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Main_Menu extends JPanel{
	
	JButton btnNewGame = new JButton("Nuevo Juego");
	JButton btnConfiguration = new JButton("Configuración");
	JButton btnMaximunScore = new JButton("Puntuación Maxima");
	JButton btnExit = new JButton("Salir");
	
	public Main_Menu(){
		JPanel panelComponents = new JPanel();
		JPanel titlePanel = new JPanel();
		titlePanel.setLayout(new FlowLayout());
		JLabel lblTitle = new JLabel("ODISEA ESPACIAL");
		Font auxFont = lblTitle.getFont();
		lblTitle.setFont(new Font(auxFont.getFontName(), auxFont.getStyle(), 50));
		titlePanel.add(lblTitle);
		panelComponents.add(btnNewGame);
		panelComponents.add(btnConfiguration);
		panelComponents.add(btnMaximunScore);
		panelComponents.add(btnExit);
		setLayout(new BorderLayout());
		add(titlePanel, BorderLayout.NORTH);
		add(panelComponents, BorderLayout.CENTER);
	}
}
