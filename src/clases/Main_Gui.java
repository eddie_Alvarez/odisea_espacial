package clases;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main_Gui {
	
	JFrame mainFrame = new JFrame("Odisea Espacial");
	JPanel panelCard = new JPanel();
	CardLayout card = new CardLayout();
	
	public Main_Gui(){
		panelCard.setLayout(card);
		mainFrame.setContentPane(panelCard);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setSize(1150, 660);
		//mainFrame.isResizable(false);
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
	}
	
	public void addPane(JPanel panel, String id){
		panelCard.add(panel, id);
	}
	
	public void showPane(String name){
		card.show(panelCard, name);
	}
	
}
