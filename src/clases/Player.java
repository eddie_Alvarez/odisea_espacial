package clases;

public class Player {
	
	private String name;
	private int punctuation;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPunctuation() {
		return punctuation;
	}
	public void setPunctuation(int punctuation) {
		this.punctuation = punctuation;
	}
	
	public Player(String name, int punctuation) {
		super();
		this.name = name;
		this.punctuation = punctuation;
	}
	
}
